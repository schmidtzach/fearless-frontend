window.addEventListener('DOMContentLoaded', async function() {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {

        const data = await response.json();
        // console.log(data)

        const selectTag = document.getElementById('location');
        // console.log(selectTag)

        for (let location of data.locations) {
            // console.log(state)
            let newElem = document.createElement('option');
            // console.log(newElem)
            newElem.value = location.id
            // console.log(newElem)
            newElem.innerHTML = location.name

            selectTag.appendChild(newElem);
        }

        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const locationUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();

            }

        })


    } else {
        console.error('Bad Repsonse')
    }


})
